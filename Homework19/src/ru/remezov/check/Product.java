package ru.remezov.check;

public class Product {
    String name;
    double price;
    double mass;

    public Product(String name, double price, double mass) {
        this.name = name;
        this.price = price;
        this.mass = mass;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public double getMass() {
        return mass;
    }
}
